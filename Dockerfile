# build stage
FROM ocaml/opam2 as build-stage
RUN sudo apt-get install -qq m4 protobuf-compiler pkg-config libcurl4-gnutls-dev libgmp-dev
#RUN opam install benchmark camlp5 cppo dune.1.11.4 jingoo markup num ounit ocurl piqi piqilib redis redis-sync stdlib-shims unidecode.0.2.0 uucp uutf yojson zarith
RUN opam install benchmark camlp5 cppo dune.1.11.4 jingoo markup stdlib-shims num unidecode uucp zarith
RUN git clone https://github.com/geneweb/geneweb --depth 1
WORKDIR geneweb
# TODO add option to support multiple config
# ocaml ./configure.ml --sosa-zarith
# ocaml ./configure.ml --api
# etc.
#RUN export PATH=$OPAM_SWITCH_PREFIX/bin/bin/:$PATH && ocaml ./configure.ml && make distrib
RUN export PATH=/home/opam/.opam/4.10/bin:$PATH && ocaml ./configure.ml && make distrib
# TODO: get rid of hardcoded opam path
# try just `export PATH=$OPAM_SWITCH_PREFIX/bin:$PATH`

# production stage
FROM debian as production-stage
# This is where your data is
ENV GENEWEBDB   /genewebData
ENV LANGUAGE    en
ENV FAMILY      lastname
ENV PORT        2317
RUN mkdir -p /home/GW
COPY --from=build-stage /home/opam/opam-repository/geneweb/distribution /home/GW
COPY bootstrap.sh /
COPY global-access.auth /
RUN chmod a+x /bootstrap.sh

VOLUME ${GENEWEBDB}

EXPOSE 2317
CMD ["/bootstrap.sh"]
