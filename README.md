A dockerfile to build and run [geneweb](https://github.com/geneweb/geneweb)


## run
`FAMILY` generates if not exist and otherwise locks the website to this family tree
```shell script
docker run --env LANGUAGE=fr --env FAMILY=merot --name geneweb -p2317:2317 -v ${HOME}/GenealogyData:/genewebData geneweb:latest
```

## TODO
- limit to FAMILY access only (cf. https://geneweb.tuxfamily.org/wiki/access)
